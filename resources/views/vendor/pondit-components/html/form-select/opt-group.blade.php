@foreach($optGroups as $optGroup)
    <optgroup label="{{$optGroup['name']}}">
        @foreach($optGroup['options'] as $option)
            <option
                value="{{$option['name']}}"
                {{ $isSelected($option['name']) ? 'selected="selected"' : '' }}
                {{ $isDisabled($option['name']) ? 'disabled="disabled"' : '' }}
            >
                {{$option['name']}}
            </option>
        @endforeach
    </optgroup>
@endforeach
