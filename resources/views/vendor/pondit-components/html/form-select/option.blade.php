
@foreach($options as $option)
    <option
        value="{{$option['id']}}"
        {{ $isSelected($option['name']) ? 'selected="selected"' : '' }}
        {{ $isDisabled($option['name']) ? 'disabled="disabled"' : '' }}
    >
        {{$option['name']}}
    </option>
@endforeach

