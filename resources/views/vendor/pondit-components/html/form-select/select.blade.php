
    <label for="">{{$labelTitle}}</label>
    <select id="{{$id}}" name="{{$name}}" {{$attributes->merge(['class'=>''])}} >

        {{$slot}}

    </select>

