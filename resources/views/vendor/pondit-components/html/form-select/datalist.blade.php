

    <label for="{{$id}}">{{$labelTitle}}</label>
    <input {{$attributes->merge([
                                'class'=>$class,
                                'list'=>$listName,
                                'name'=>$name,
                                'id'=>$id,
                            ])}} >

    <datalist id="{{$listName}}">
        @foreach($options as $option)
            <option value="{{$option['name']}}" >
        @endforeach
    </datalist>


