<div class="form-group">
    {{$slot}}
    {{--    <x-admin.form-components.pondit-form-label-select2 class="" >BASIC SELECT</x-admin.form-components.pondit-form-label-select2>--}}
    <select class="{{$class}}" data-fouc>
        <optgroup label="Mountain Time Zone">
            <option value="AZ">Arizona</option>
            <option value="CO">Colorado</option>
            <option value="ID">Idaho</option>
            <option value="WY">Wyoming</option>
        </optgroup>
        <optgroup label="Central Time Zone">
            <option value="AL">Alabama</option>
            <option value="AR">Arkansas</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
        </optgroup>
    </select>
</div>
