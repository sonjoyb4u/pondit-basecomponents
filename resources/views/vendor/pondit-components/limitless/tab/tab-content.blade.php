<div class="tab-content">
    <div {{$attributes->merge(['class'=>'tab-pane fade'])}} id="basic-tab1">
        Basic tabs example using <code>.nav-tabs</code> class. Also requires base <code>.nav</code> class.
    </div>

    <div class="tab-pane fade" id="basic-tab2">
        Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
    </div>

    <div class="tab-pane fade" id="basic-tab3">
        DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
    </div>

    <div class="tab-pane fade" id="basic-tab4">
        Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
    </div>
</div>
