
    <label for="">{{$labelTitle}}</label>
    <select id="{{$id}}" name="{{$name}}" {{$attributes->merge(['class'=>''])}} >
        @if(isset($option_tag))
            {{$option_tag}}
        @endif

        {{$slot}}

    </select>

