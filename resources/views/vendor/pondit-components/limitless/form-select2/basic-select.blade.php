<div class="form-group">
    {{$slot}}
    {{--    <x-admin.form-components.pondit-form-label-select2 class="" >BASIC SELECT</x-admin.form-components.pondit-form-label-select2>--}}
    <select class="{{$class}}" data-fouc>
        @if(isset($dataArray))
            <optgroup label="Mountain Time Zone">
                @foreach($dataArray as $key => $val)
                <option value="{{$key}}">{{$val}}</option>
                @endforeach
            </optgroup>
        @endif

    </select>
</div>
