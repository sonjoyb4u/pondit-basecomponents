<x-admin.layouts.app >

@push('js')
    {{-- Select2 Js...--}}
    <script src="{{asset('admin/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/form_select2.js')}}"></script>
@endpush

{{--    Page header--}}
        <x-admin.view.page-header >

            <x-admin.view.breadcrumb >
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="form_select2.html" class="breadcrumb-item">Forms</a>
                <span class="breadcrumb-item active">Select2 selects</span>
            </x-admin.view.breadcrumb>

            <x-admin.view.page-title >
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Forms</span> - Select2
                    Selects</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </x-admin.view.page-title>

        </x-admin.view.page-header>
{{--    End page header--}}

        <div class="content">
            {{--SELECT2 SINGLE/MULTIPLE SELECT COMPONENTS...--}}
            <div class="row">

                <div class="col-md-6">
                    <x-pondit-bl-dv-card card-title="Single Select">

                        @php
                            $option_groups = [
                                [
                                    'id' => 1,
                                    'name' => 'Mountain Time Zone',
                                    'options' => [
                                          [
                                            'id' => 1,
                                            'name' => 'Lorem 1',
                                          ],
                                          [
                                            'id' => 2,
                                            'name' => 'Ipsam 1',

                                          ],
                                          [
                                            'id' => 3,
                                            'name' => 'Doler 1',
                                          ],
                                          [
                                            'id' => 4,
                                            'name' => 'Sit 1',
                                          ]

                                    ]
                                ],
                                [
                                    'id' => 2,
                                    'name' => 'Central Time Zone',
                                    'options' => [
                                          [
                                            'id' => 1,
                                            'name' => 'Lorem 2',
                                          ],
                                          [
                                            'id' => 2,
                                            'name' => 'Ipsam 2',

                                          ],
                                          [
                                            'id' => 3,
                                            'name' => 'Doler 2',
                                          ],
                                          [
                                            'id' => 4,
                                            'name' => 'Sit 2',
                                          ]

                                    ]
                                ],
                                [
                                    'id' => 3,
                                    'name' => 'Local Time Zone',
                                    'options' => [
                                          [
                                            'id' => 1,
                                            'name' => 'Lorem 3',
                                          ],
                                          [
                                            'id' => 2,
                                            'name' => 'Ipsam 3',

                                          ],
                                          [
                                            'id' => 3,
                                            'name' => 'Doler 3',
                                          ],
                                          [
                                            'id' => 4,
                                            'name' => 'Sit 3',
                                          ]

                                    ]
                                ],

                            ];

                            $option_group = [
                                  'id' => 3,
                                  'name' => 'Local Time Zone',
                                  'option' => [
                                        'id' => 4,
                                        'name' => 'Sit 3',
                                  ]

                            ];

                            $options = [
                                  [
                                    'id' => 1,
                                    'name' => 'Lorem 1',
                                  ],
                                  [
                                    'id' => 2,
                                    'name' => 'Ipsam 1',

                                  ],
                                  [
                                    'id' => 3,
                                    'name' => 'Doler 1',
                                  ],
                                  [
                                    'id' => 4,
                                    'name' => 'Sit 1',
                                  ]

                            ];

                            $option = [
                                'id' => 2,
                                'name' => 'Ipsam 1',
                            ];

                            $option_groups_icons = [

                                [
                                    'id' => 1,
                                    'name' => 'File types',
                                    'options' => [
                                          [
                                            'id' => 1,
                                            'icon' => 'file-pdf',
                                            'name' => 'pdf',
                                          ],
                                          [
                                            'id' => 2,
                                            'icon' => 'file-word',
                                            'name' => 'word',
                                          ],
                                          [
                                            'id' => 3,
                                            'icon' => 'file-excel',
                                            'name' => 'excel',
                                          ],
                                          [
                                            'id' => 4,
                                            'icon' => 'file-openoffice',
                                            'name' => 'openoffice',
                                          ]

                                    ]
                                ],
                                [
                                    'id' => 2,
                                    'name' => 'Browsers',
                                    'icons' => ['chrome', 'firefox', 'opera', 'IE', 'safari'],
                                    'options' => [
                                          [
                                            'id' => 1,
                                            'icon' => 'chrome',
                                            'name' => 'chrome',
                                          ],
                                          [
                                            'id' => 2,
                                            'icon' => 'firefox',
                                            'name' => 'firefox',

                                          ],
                                          [
                                            'id' => 3,
                                            'icon' => 'opera',
                                            'name' => 'opera',
                                          ],
                                          [
                                            'id' => 4,
                                            'icon' => 'IE',
                                            'name' => 'ie',
                                          ],
                                          [
                                            'id' => 5,
                                            'icon' => 'safari',
                                            'name' => 'safari',
                                          ]

                                    ]
                                ],

                            ];

                            $option_group_icon = [
                                  'id' => 2,
                                  'name' => 'Browsers',
                                  'option' => [
                                      'id' => 4,
                                      'icon' => 'IE',
                                      'name' => 'ie',
                                  ]

                            ];
                        @endphp

                        <x-pondit-wl-de-sls2w>
                            <x-pondit-be-de-s label-title="Options Title Name">
                                <x-slot name="option_tag">
                                    <option>Select a Option...</option>
                                </x-slot>
                                <x-pondit-be-de-option :options="$options" />
                            </x-pondit-be-de-s>
                        </x-pondit-wl-de-sls2w>

                        <x-pondit-wl-de-sls2w>
                            <x-pondit-be-de-s label-title="Option-Groups Title Name">
                                <x-pondit-be-de-optgroup :opt-groups="$option_groups" />
                            </x-pondit-be-de-s>
                        </x-pondit-wl-de-sls2w>

                        <x-pondit-wl-de-sls2w>
                            <x-pondit-be-de-datalist
                                id="datalist_id"
                                name="datalist_name"
                                list-name="ListName"
                                label-title="Select Datalist"
                                :options="$options" >

                            </x-pondit-be-de-datalist>
                        </x-pondit-wl-de-sls2w>

                        <x-pondit-wl-de-sls2w>
                            <x-pondit-bl-de-s2
                                label-title="Select2 Option"
                                data-placeholder="Select a Option..."
                                class="form-control select ">

                                <x-slot name="option_tag">
                                    <option></option>
                                </x-slot>
                                <x-pondit-bl-de-options :options="$options" :selected="$option['name']" />

                            </x-pondit-bl-de-s2>
                        </x-pondit-wl-de-sls2w>


                    </x-pondit-bl-dv-card>

                </div>

                @php
                    $ext_numbers = [
                                        [
                                          'id'=> 1,
                                          'name'=> '+02',
                                        ],
                                        [
                                          'id'=> 2,
                                          'name'=> '+031',
                                        ],
                                        [
                                          'id'=> 3,
                                          'name'=> '+88',
                                        ],
                                        [
                                          'id'=> 4,
                                          'name'=> '+99',
                                        ],
                                    ]
                @endphp

                <div class="col-md-6">
                    <x-pondit-bl-dv-card card-title="Limitless Wrapper wise Functional Component">

                        <x-pondit-fe-de-bdnumber :options="$ext_numbers" />

                    </x-pondit-bl-dv-card>

                </div>

            </div>

{{--        TABS COMPONENTS...--}}
            <div class="row">

                <div class="col-md-6">
{{--                    <x-pondit-bl-ve-card card-title="Basic Tabs">--}}
{{--                            @php--}}

{{--                            @endphp--}}

{{--                            <x-pondit-bl-ve-tabs--}}
{{--                                class="">--}}

{{--                                <x-pondit-bl-ve-tab-content--}}
{{--                                    class="show active"/>--}}

{{--                            </x-pondit-bl-ve-tabs>--}}

{{--                    </x-pondit-bl-ve-card>--}}
                </div>

            </div>

        </div>
{{--    End Content...--}}

</x-admin.layouts.app>







