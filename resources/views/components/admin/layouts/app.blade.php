<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PONDIT || @yield('title', '')</title>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/global_assets/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    @stack('css')
    <!-- Core JS files -->
    <script src="{{asset('admin/global_assets/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->
    <!-- Theme JS files -->
    <script src="{{asset('admin/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    @stack('js')

    <script src="{{asset('admin/assets/js/app.js')}}"></script>
    <script src="{{asset('admin/global_assets/js/demo_pages/dashboard.js')}}"></script>
    <!-- /theme JS files -->
</head>
<body>
<!-- Main navbar -->
<x-admin.partials.navbar />
<!-- /main navbar -->

<!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <x-admin.partials.sidebar />
        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">

            {{ $slot }}

            <!-- Footer -->
            <x-admin.partials.footer />
            <!-- /footer -->
        </div>
        <!-- /main content -->
    </div>
<!-- /page content -->
</body>
</html>

