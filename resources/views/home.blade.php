<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            div {
                width: 100%;
                background-color: black;
                margin: 0 auto;
            
            }
            ul {
                width: 30%;
                display: block;
                margin: 0 auto;
                padding: 15px;
                                            
            } 
            li {
                display: inline-block;
                padding: 15px;

            }
            li a {
                text-decoration: none;
                color: #fff
            }
        </style>

    </head>
    <body class="antialiased">
        {{-- <div class="relative flex items-top justify-center min-h-screen sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif


            
        </div> --}}
        <div>
            <ul>
                <li><a href="{{url('/rikta')}}">Rikta</a></li>
                <li><a href="{{url('/oishee')}}">Oishee</a></li>
                <li><a href="{{url('/noushin')}}">Noushin</a></li>
                <li><a href="{{url('/sonjoy')}}">Sonjoy</a></li>
                <li><a href="{{url('/tanju')}}">Tanju</a></li>
            </ul>
        </div>
{{--        <x-pondit-bh-in-text placeholder="{{__('widgets::lang.enter your name')}}"/>--}}
    </body>
</html>
