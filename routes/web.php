<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/rikta', function () {
    return view('team-members.rikta');
});
Route::get('/oishee', function () {
    return view('team-members.oishee');
});
Route::get('/noushin', function () {
    return view('team-members.noushin');
});
Route::get('/sonjoy', function () {
    return view('team-members.sonjoy');
});
Route::get('/tanju', function () {
    return view('team-members.tanju');
});