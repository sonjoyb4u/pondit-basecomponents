<?php

namespace Pondit\BaseComponents\DataEntry\HtmlForm;

use Illuminate\View\Component;

class TextInput extends Component
{
    public 
           $id
           ,$class
           ,$placeholder
           ,$name;

    public function __construct
    (
        $id     = false
        ,$class = false
        ,$name  = false 
        ,$placeholder  = false 
    )
    {
        $this->id       = $id;
        $this->class    = $class;
        $this->name     = $name;
        $this->placeholder     = $placeholder;
    }
    
    public function render()
    {
        return view('widgets::html.text-input');
    }

}
