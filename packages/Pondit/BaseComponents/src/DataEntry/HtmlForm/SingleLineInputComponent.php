<?php

namespace Pondit\BaseComponents\DataEntry\HtmlForm;

use Illuminate\View\Component;

class SingleLineInputComponent extends Component
{
    public $label=null, $type=null, $class=null,  $id=null, $name=null, $value=null, $placeholder=null;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $uuid = \Illuminate\Support\Str::uuid();
//        $this->id = "id_" . $uuid;
        $this->id = "id_" . $this->uUId();
        $this->name = "name_" . $this->uUId();

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('widgets::html.single-line-input-component');
    }

    public function uUId()
    {
        $uuid = \Illuminate\Support\Str::uuid();
        return $uuid;
    }
}
