<?php

namespace Pondit\BaseComponents\DataEntry\HtmlForm\DropDown;

use Illuminate\View\Component;

class Select extends Component
{
    public $label, $id, $name, $placeholder;


    public function __construct
    (
        $label = false,
        $id = false,
        $name = false,
        $placeholder = false
    )
    {
        $this->label    = $label;
        $this->id    = $id;
        $this->name    = $name;
        $this->placeholder    = $placeholder;
    }

    public function render()
    {
        return view('widgets::html.form-select.select');
    }

}


