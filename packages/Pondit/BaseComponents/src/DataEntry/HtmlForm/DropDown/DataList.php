<?php

namespace Pondit\BaseComponents\DataEntry\HtmlForm\DropDown;

use Illuminate\View\Component;

class DataList extends Component
{
    public $id, $name, $class, $labelTitle, $listName, $options;


    public function __construct
    (
        $id = false,
        $name = false,
        $class = false,
        $labelTitle = false,
        $listName = false,
        $options = false
    )
    {
        $this->id    = $id;
        $this->name    = $name;
        $this->class    = $class;
        $this->labelTitle    = $labelTitle;
        $this->listName    = $listName;
        $this->options    = $options;
    }

    public function render()
    {
        return view('widgets::html.form-select.datalist');
    }

}



