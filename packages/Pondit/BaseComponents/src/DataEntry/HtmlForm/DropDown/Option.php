<?php

namespace Pondit\BaseComponents\DataEntry\HtmlForm\DropDown;

use Illuminate\View\Component;

class Option extends Component
{
    public $id, $key, $options, $optGroups, $optGrp, $selected, $disabled;

    public function __construct
    (
        $key = false,
        $options = false,
        $optGroups = false,
        $optGrp = false,
        $selected = false,
        $disabled = false
    )
    {
        $this->key    = $key;
        $this->options    = $options;
        $this->optGroups    = $optGroups;
        $this->optGrp    = $optGrp;
        $this->selected    = $selected;
        $this->disabled    = $disabled;
    }


    /**
     * Determine if the given option is the currently selected option.
     *
     * @param  string  $option
     * @return bool
     */
    public function isSelected($option)
    {
        return $option === $this->selected;
    }

    /**
     * Determine if the given option is the currently selected option.
     *
     * @param  string  $option
     * @return bool
     */
    public function isDisabled($option)
    {
        return $option === $this->disabled;
    }


    public function render()
    {
        return view('widgets::html.form-select.option');
    }

}


