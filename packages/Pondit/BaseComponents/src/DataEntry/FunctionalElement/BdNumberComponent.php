<?php

namespace Pondit\BaseComponents\DataEntry\FunctionalElement;

use Illuminate\View\Component;

class BdNumberComponent extends Component
{
    public $options;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($options=false)
    {
        $this->options    = $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('widgets::functional.bd-number-component');
    }
}
