<?php

namespace Pondit\BaseComponents\DataEntry\LimitlessForm;

use Illuminate\View\Component;

class SingleLineInputWrapper extends Component
{
    public $label;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($label=false)
    {
        $this->label    = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('widgets::limitless.single-line-input-wrapper');
    }
}
