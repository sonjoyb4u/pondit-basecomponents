<?php

namespace Pondit\BaseComponents\DataEntry\LimitlessForm\DropDown;

use Illuminate\View\Component;

class Select2 extends Component
{
    public $labelTitle, $id, $name, $placeHolder;


    public function __construct
    (
        $labelTitle = false,
        $id = false,
        $name = false,
        $placeHolder = false
    )
    {
        $this->labelTitle    = $labelTitle;
        $this->id    = $id;
        $this->name    = $name;
        $this->placeHolder    = $placeHolder;
    }

    public function render()
    {
        return view('widgets::limitless.form-select2.select2');
    }

}

