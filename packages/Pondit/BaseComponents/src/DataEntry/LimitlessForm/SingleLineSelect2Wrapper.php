<?php

namespace Pondit\BaseComponents\DataEntry\LimitlessForm;

use Illuminate\View\Component;

class SingleLineSelect2Wrapper extends Component
{

    public function __construct
    (

    )
    {

    }

    public function render()
    {
        return view('widgets::limitless.single-line-select2-wrapper');
    }

}




