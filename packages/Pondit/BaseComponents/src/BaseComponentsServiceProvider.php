<?php
namespace Pondit\BaseComponents;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
//use Pondit\BaseComponents\Components\Html\DataEntry\TextInput;


class BaseComponentsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadComponents();

        $this->loadTranslationsFrom(__DIR__.'/Resources/lang', 'widgets');
    }

    public function register()
    {
        $this->publishers();
        $this->loadViewsFrom(__DIR__.'/Resources/components', 'widgets');
    }

    private function publishers()
    {

        $this->publishes([
            __DIR__.'/Resources/lang' => resource_path('lang/vendor/widgets'),
        ]);

        $this->publishes([
            __DIR__.'/Resources/components' => resource_path('views/vendor/pondit-components'),
        ], 'pondit-components');

        $this->publishes([
            __DIR__.'/Publishable' => public_path('vendor/pondit-component'),
        ], 'pondit-assets');

    }

    private function loadComponents()
    {

         /**
         * HTML WiSE COMPONENTS
         * ------ abbreviation ------
         * ['in => input', ' ']
         */
        Blade::component('pondit-bh-in-text', DataEntry\HtmlForm\TextInput::class);


         /**
         * PONDIT TEAM MEMBERS
         */

         /**
          * MEMBERS =>  RIKTA
          */




















         /**
          * MEMBERS =>  OISHEE
          */


























         /**
          * MEMBERS =>  NOUSHIN
          */






















        /**
         * HTML, BOOTSTRAP, LIMITLESS WiSE COMPONENTS TAG NAME
         * ------ Abbreviation ------
         * [
         * 'dv' => 'data view', 'bh' => 'base html', 'bl' => 'base limitless',
         * 'de' => 'data entry', 'dd' => 'dropdown', 'slic' => 'single line input component',
         * 'sliw' => 'single line input wrapper', 'sls2w' => 'single line select2 wrapper',
         * 's' => 'select', 's2' => 'select2', 'ph' => 'placeholder', 'ti' => 'text-input',
         * ]
         */
         /**
          * MEMBERS =>  SONJOY
          */

//        BASE ELEMENT HTML SELECT...
        Blade::component('pondit-be-de-slic',
            DataEntry\HtmlForm\SingleLineInputComponent::class);
        Blade::component('pondit-be-de-s',
            DataEntry\HtmlForm\DropDown\Select::class);
        Blade::component('pondit-be-de-option',
            DataEntry\HtmlForm\DropDown\Option::class);
        Blade::component('pondit-be-de-optgroup',
            DataEntry\HtmlForm\DropDown\OptGroup::class);
        Blade::component('pondit-be-de-ti',
            DataEntry\HtmlForm\TextInput::class);
        Blade::component('pondit-be-de-datalist',
            DataEntry\HtmlForm\DropDown\DataList::class);
//        WRAPPER LIMITLESS...
        Blade::component('pondit-bl-dv-card',
            DataView\LimitlessView\Card::class);
        Blade::component('pondit-wl-de-sliw',
            DataEntry\LimitlessForm\SingleLineInputWrapper::class);
        Blade::component('pondit-wl-de-sls2w',
            DataEntry\LimitlessForm\SingleLineSelect2Wrapper::class);
//        BASE ELEMENT LIMITLESS SELECT2...
        Blade::component('pondit-bl-de-s2',
            DataEntry\LimitlessForm\DropDown\Select2::class);
        Blade::component('pondit-bl-de-options',
            DataEntry\LimitlessForm\DropDown\Options::class);
        Blade::component('pondit-bl-de-ph-options',
            DataEntry\LimitlessForm\DropDown\PlaceholderOptions::class);
//        FUNCTIONAL ELEMENT...
        Blade::component('pondit-fe-de-bdnumber',
            DataEntry\FunctionalElement\BdNumberComponent::class);
        Blade::component('pondit-bl-ve-tabs',
            DataView\LimitlessView\Tab\Tabs::class);
        Blade::component('pondit-bl-ve-tab-content',
            DataView\LimitlessView\Tab\TabContent::class);


























         /**
          * MEMBERS =>  TANJU
          */





















    }
}
