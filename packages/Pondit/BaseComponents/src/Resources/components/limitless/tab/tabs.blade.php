
    <ul {{$attributes->merge(['class'=>'nav nav-tabs'])}}>
        <li class="nav-item">
            <a href="#basic-tab1" class="nav-link active" data-toggle="tab">Home</a>
        </li>
        <li class="nav-item">
            <a href="#basic-tab2" class="nav-link" data-toggle="tab">About</a>
        </li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Blog</a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="#basic-tab3" class="dropdown-item" data-toggle="tab">Article One</a>
                <a href="#basic-tab4" class="dropdown-item" data-toggle="tab">Article Two</a>
            </div>
        </li>
    </ul>


    {{ $slot }}

