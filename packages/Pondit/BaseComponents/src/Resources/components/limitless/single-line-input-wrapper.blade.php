
<div class="form-group row">
    @if(@isset($label) && !@empty($label))
     <label class="col-form-label col-lg-2 ">{{$label}}</label>
    @endif
    <div class="col-lg-10">
        {{$slot}}
    </div>
</div>
