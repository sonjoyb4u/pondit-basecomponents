
    @if(!empty($optGroups))
        @foreach($optGroups as $optGroup)
            <optgroup label="{{$optGroup['name']}}">
                @foreach($optGroup['options'] as $option)
                    @if(empty($option['icon']))
                        <option
                            value="{{$option['name']}}"
                            {{ $isSelected($option['name']) ? 'selected="selected"' : '' }}
                            {{ $isDisabled($option['name']) ? 'disabled="disabled"' : '' }}
                        >
                            {{$option['name']}}
                        </option>
                    @else
                        <option
                            value="{{$option['name']}}"
                            data-icon="{{$option['icon']}}"
                            {{ $isSelected($option['name']) ? 'selected="selected"' : '' }}
                            {{ $isDisabled($option['name']) ? 'disabled="disabled"' : '' }}
                        >
                            {{$option['name']}}
                        </option>
                    @endif
                @endforeach
            </optgroup>
        @endforeach
    @else
        @foreach($options as $option)
            <option
                value="{{$option['id']}}"
                {{ $isSelected($option['name']) ? 'selected="selected"' : '' }}
                {{ $isDisabled($option['name']) ? 'disabled="disabled"' : '' }}
            >
                {{$option['name']}}
            </option>
        @endforeach
    @endif


{{--@if($optGrp)--}}
{{--    {{ $option['id'] === $optGrp['options']['id'] ? 'selected' : '' }}--}}
{{--@endif--}}


