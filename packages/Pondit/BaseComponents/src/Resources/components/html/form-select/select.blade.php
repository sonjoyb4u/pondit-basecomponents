    @if(isset($label) && !empty($label))
    <label for="">{{$label}}</label>
    @endif
    <select id="{{$id}}" name="{{$name}}" {{$attributes->merge(['class'=>''])}} >
        @if(isset($option_tag))
            {{$option_tag}}
        @endif
        {{$slot}}
    </select>

