<?php

namespace Pondit\BaseComponents\DataView\LimitlessView\Tab;

use Illuminate\View\Component;

class Tabs extends Component
{
    public $labelText;


    public function __construct
    (
        $labelTitle = false
    )
    {
        $this->labelTitle    = $labelTitle;
    }

    public function render()
    {
        return view('widgets::limitless.tab.tabs');
    }

}

