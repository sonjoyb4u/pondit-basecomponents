<?php

namespace Pondit\BaseComponents\DataView\LimitlessView\Tab;

use Illuminate\View\Component;

class TabContent extends Component
{
    public $labelTitle;


    public function __construct
    (
        $labelTitle = false
    )
    {
        $this->labelTitle    = $labelTitle;
    }

    public function render()
    {
        return view('widgets::limitless.tab.tab-content');
    }

}


