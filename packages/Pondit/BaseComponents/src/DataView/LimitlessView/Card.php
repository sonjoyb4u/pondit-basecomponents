<?php

namespace Pondit\BaseComponents\DataView\LimitlessView;

use Illuminate\View\Component;

class Card extends Component
{
    public $cardTitle;


    public function __construct
    (
        $cardTitle = false
    )
    {
        $this->cardTitle    = $cardTitle;
    }

    public function render()
    {
        return view('widgets::limitless.card');
    }

}

